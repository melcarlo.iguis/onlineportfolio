const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');
const auth = require('../auth');

// register route
router.post('/register' , (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// login route
router.post('/login' , (req,res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

module.exports = router;