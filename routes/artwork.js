const express = require('express');
const router = express.Router();
const artworkController = require('../controllers/artwork');
const auth = require('../auth');
const {cloudinary} = require('../utils/cloudinary');
const upload = require('../utils/multer');

// -  Upload Artwork (Admin only) - routes
router.post('/create', upload.single('image') , auth.verify, async  (req, res) => {	
	const userData = await auth.decode(req.headers.authorization)
	const result = await  cloudinary.uploader.upload(req.file.path, {
		upload_preset: 'online_portfolio'
	})
	if(userData.isAdmin) {
		artworkController.uploadArtwork(req, result).then(resultFromController=> res.send(resultFromController))
	}else{
		return res.send("you're not an admin!, failed to add artwork")
	}
})


// - Retrieve all active artwork - routes
router.get('/active' , (req, res) =>{
	artworkController.getAllActive().then(resultFromController => res.send(resultFromController))
})

// - Retrieve all  products - routes
router.get("/all", (req,res)=> {
	artworkController.allArtworks().then(resultFromController=>
		res.send(resultFromController))
})	
// - Archive Product (Admin only) - routes
router.put('/:productId/archive' , (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		artworkController.archiveProduct(req.params, req.body).then(resultFromController=> res.send(resultFromController))
	}else{
		return res.send("you're not an admin!, failed to archive product")
	}
})

module.exports = router;