const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// controller for creating new user
module.exports.registerUser = (reqBody) =>{

	let newUser = new User({
		username: reqBody.username,
		password: bcrypt.hashSync(reqBody.password, 10),

	})

	// condition for checking if the email already exist.
	return User.find({username : reqBody.username}).then(result => {

		if(result.length > 0 ){
			console.log("user already exist");
			return("user already exist, failed to register")

		}else{
			return newUser.save().then((user, error)=>{
				if(error){
					console.log("failed to register")
					return false
				}else{
					console.log("successfully register")
					return true
					
				}
			})
		}
	})
}


// login controller
module.exports.loginUser = (reqBody) => {

	return User.findOne({username : reqBody.username}).then(result => {
		if(result == null){
			return false
		}else{

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			}else{
				return false
			}
		}
	})
}
