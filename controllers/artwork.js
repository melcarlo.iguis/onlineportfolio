const User = require('../models/User');
const Artwork = require('../models/Artwork');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// -  Upload Artwork
module.exports.uploadArtwork = (req, result) => {

	console.log(result)
	console.log(req.body.category)

	let newArtwork = new Artwork({

		category: req.body.category,
		picture: result.secure_url ,
		cloudinary_id: result.public_id

	})

	return newArtwork.save().then((addArtwork,error)=> {
		if(error){
			return res.status(400).json({error});
		}else if(addArtwork){
			console.log("Artwork has been added")
			return ({addArtwork})

		}
	})

}

// - Retrieve all active products controller
module.exports.getAllActive = () => {
	return Artwork.find({isActive: true}).then(result =>{
		return result
	})
}

// - Retrieve all  products controller
module.exports.allArtworks = ()=>{
	return Artwork.find().then(result=> {
		return result
	})
}
