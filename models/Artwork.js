const mongoose = require('mongoose')
const artWorkSchema = new mongoose.Schema({

	picture : {
		type: String,
		required : [true, 'picture is required']
	},

	cloudinary_id : {
		type: String,
		required : [true, 'cloudinary id is required']
	},

	category : {
		type: String,
		required : [true, 'Category is required']
	},

	isActive : {
		type: Boolean,
		default : true
	},

	createdOn : {
		type: Date,
		default : new Date()
	}

})

module.exports = mongoose.model('Artwork', artWorkSchema)